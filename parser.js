var { DOMParser } = require('xmldom');
var xpath = require('xpath');


const DEFAULT_COMPONENT_NAME = 'DefaultComponent';
const DEFAULT_CONTEXT_NAME = 'DefaultContext'
const DEFAULT_EXTENDER_NAME = 'templates';
const DEFAULT_FUNCTION_NAME = 'render';
const CLASS_TAGS = 'render';
const EXT_TAGS = 'render';
const JTM_TAGS = 'assign|class|context|export|extender|import|sfc';
const USE_ATTR = '@*[starts-with(name(), \'use\')]';

var re =
{
    jsxBody: { match: /(<(render|sfc)[\s\S]*?>)([\s\S]+?)(<\/(render|sfc)\s*>)/g, replace: '$1<![CDATA[$3]]>$4' },

    jsxSwaps:
    [
        // to swap 'class' attribute for 'className'
        { match: /\s+class\s*=\s*(\{|")/g, replace: ' className=$1' },
        // to swap 'for' attribute for 'htmlFor'
        { match: /\s+for\s*=\s*(\{|")/g, replace: ' htmlFor=$1' },
    ],

    xmlWrap: /^<jsx-templates[\s\S]*?>/,
}

var strings = (step = 0, lines = []) =>
{
    var indent = '    '.repeat(step);

    var face =
    {
        join: delim => lines.join(delim || '\n'),

        push: strings =>
        {
            [].concat(strings).forEach(str => lines.push(indent + str));
            return face;
        },

        sub: next => strings(step + next, lines),

        subFn: (next, fn) => fn(face.sub(next))
    }

    return face;
}

module.exports = content =>
{
    var defaultExportName = null, exportArray = [];
    // swap some valid html strings to make them work in jsx
    var attrSwap = string => re.jsxSwaps.reduce((str, swap) => str.replace(swap.match, swap.replace), string)
    // return a specifed node attribute value or `def` if non-existent
    var attrValue = (node, name, def) => (xpath.select('@' + name, node)[0] || {}).value || def
    // get node internal body content
    var bodyContent = node => attrSwap(xpath.select('text()', node)[0].data)
    // add quotes for comma-delimited parts of a string
    var quoteStrs = str => str ? str.split(/\s*,\s*/).map(s => `'${s}'`).join(', ') : ''
    // return name or default name and add to exports
    var attrName = (node, defaultName) =>
    {
        var name = attrValue(node, 'name');
        var theName = name || defaultName;
        // add to exports list
        exportArray.push(theName);
        // set as default export if no name given
        if (!name) defaultExportName = theName;

        return theName;
    }


    var getBody = (body, { loop }, lines = strings()) =>
    {
        if (loop)
        {
            lines.push(`var _jsx_loop_ = ${loop};`);
            // create array from existing variable (arg or destructure)
            lines.push(`var _jsx_array_ = Array.isArray(_jsx_loop_) ? _jsx_loop_ :`);
            lines.push(`    typeof _jsx_loop_ === 'number' ? Array.apply(null, Array(_jsx_loop_)) :`);
            lines.push(`    typeof _jsx_loop_ === 'string' ? _jsx_loop_.split(/\\s*,\\s*/) :`);
            lines.push(`    typeof _jsx_loop_ === 'object' ? Object.keys(_jsx_loop_) : [_jsx_loop_];`);
            // iterate the array with the jsx body
            lines.push(`return _jsx_array_.map((item, index, array) => (${body}));`)
        }
        else
        {
            lines.push(`return (${body});`);
        }

        return lines;
    }

    var getDestructures = (items, lines = strings()) =>
    {
        for (var name in items)
        {
            if (items[name]) lines.push(`var { ${items[name]} } = ${name};`);
        }

        return lines;
    }

    var getHighOrder = (name, highOrder, lines = strings()) =>
    {
        if (highOrder) lines.push(`${name} = [${highOrder}].reduce((comp, fn) => fn(comp), ${name});`);

        return lines;
    }

    var getRootNode = string =>
    {
        // ensure string content is wrapped in expected element root
        if (!re.xmlWrap.test(string.trim())) string = `<jsx-templates>${string}</jsx-templates>`;
        // wrap jsx in CDATA sections for appropriate jtm tags
        var adapted = string.replace(re.jsxBody.match, re.jsxBody.replace);
        // parse input as xml
        var doc = new DOMParser().parseFromString(adapted, "text/xml");

        return xpath.select('/jsx-templates', doc)[0];
    }

    var getUseContexts = (names, lines = strings()) =>
    {
        names && names.split(/\s*,\s*/).forEach(name =>
        {
            var vname = name.slice(0, 1).toLowerCase() + name.slice(1) + 'Value';
            lines.push(`var ${vname} = useContext(${name});`);
        });

        return lines;
    }

    var getUseRefs = (names, lines = strings()) =>
    {
        names && names.split(/\s*,\s*/).forEach(name =>
        {
            lines.push(`var ${name} = useRef(null), ${name}Ref = ${name}.current;`);
        });

        return lines;
    }

    var getUseStates = (node, lines = strings()) =>
    {
        xpath.select(USE_ATTR, node).map(({ name, value }) =>
        {
            let base = name.slice(3), state = base.slice(0, 1).toLowerCase() + base.slice(1);
            lines.push(`var [ ${state}, set${base} ] = useState(${value});`);
        });

        return lines;
    }

    var parse = (node, tags, indent) =>
    {
        return strings().push(xpath.select(tags, node).map(item => parse[item.tagName](item, strings(indent)).join()));
    }

    parse.assign = (node, lines = strings()) =>
    {
        var vars = attrValue(node, 'vars');
        var expr = attrValue(node, 'expr');

        return lines.push(`var ${vars} = ${expr};`);
    }

    parse.class = (node, lines = strings()) =>
    {
        var name = attrName(node, DEFAULT_COMPONENT_NAME);
        var xtends = attrValue(node, 'extends', 'React.Component');
        var initProps = attrValue(node, 'initProps', '');
        var initState = attrValue(node, 'initState', '');
        var contextType = attrValue(node, 'contextType', '');
        var bind = attrValue(node, 'bind', '');
        var highOrder = attrValue(node, 'highOrder');

        lines.push(`class ${name} extends ${xtends}`);
        lines.push('{');
        lines.push(`    constructor(props)`);
        lines.push(`    {`);
        lines.push(`        super(props);`);
        lines.push(`        this.state = { ...this.state, ${initState} };`);
        if (bind) lines.push(`        [${quoteStrs(bind)}].forEach(name => this[name] = this[name].bind(this));`);
        lines.push(`    }`);
        lines.push(parse(node, CLASS_TAGS, 1).join());
        lines.push('}');
        // add default prop values if they exist
        if (initProps) lines.push(`${name}.defaultProps = { ${initProps} };`);
        // set context type if given
        if (contextType) lines.push(`${name}.contextType = ${contextType};`);
        // add HOC(s) if given
        getHighOrder(name, highOrder, lines);

        return lines;
    }

    parse.context = (node, lines = strings()) =>
    {
        var name = attrName(node, DEFAULT_CONTEXT_NAME);
        var defaultValue = attrValue(node, 'default', '');

        return lines.push(`var ${name} = React.createContext(${defaultValue});`);
    }

    parse.export = (node, lines = strings()) =>
    {
        var spec = attrValue(node, 'spec', '*');
        var from = attrValue(node, 'from');

        return lines.push(`export ${spec} from '${from}';`);
    }

    parse.extender = (node, lines = strings()) =>
    {
        var name = attrName(node, DEFAULT_EXTENDER_NAME);
        var prefix = attrValue(node, 'prefix', '');

        lines.push(`function _extend_${name}_(object)`);
        lines.push('{');
        lines.push('    var _items_ =');
        lines.push('    {');
        lines.push(parse(node, EXT_TAGS, 2).join(',\n'));
        lines.push('    };');
        lines.push(`    var _format_ = name =>`);
        lines.push('    {');
        lines.push(`        if ('${prefix}' && name !== '${prefix}')`);
        lines.push(`            return '${prefix}' + name.slice(0, 1).toUpperCase() + name.slice(1);`);
        lines.push(`        return name;`);
        lines.push('    };');
        lines.push(`    Object.keys(_items_).forEach(name => object[_format_(name)] = _items_[name]);`);
        lines.push('}');
        lines.push(`function ${name}(Component)`);
        lines.push('{');
        lines.push(`    _extend_${name}_(Component.prototype);`);
        lines.push(`    return Component;`);
        lines.push('}');
        lines.push(`_extend_${name}_(${name});`);

        return lines;
    }

    parse.import = (node, lines = strings()) =>
    {
        var spec = attrValue(node, 'spec');
        var from = attrValue(node, 'from');

        return lines.push('import ' + (spec ? `${spec} from ` : '') + `'${from}';`);
    }

    parse.render = (node, lines = strings()) =>
    {
        var name = attrValue(node, 'name', DEFAULT_FUNCTION_NAME);
        var bind = attrValue(node, 'bind');
        var args = attrValue(node, 'args', '');
        var siht = attrValue(node, 'this', '');
        var klass = attrValue(node, 'klass', '');
        var props = attrValue(node, 'props', '');
        var state = attrValue(node, 'state', '');
        var context = attrValue(node, 'context', '');
        var loop = attrValue(node, 'loop');
        var body = bodyContent(node);
        // render function signature
        lines.push(bind ? `${bind} = (${args}) =>` : `${name}(${args})`);
        // opening function brace
        lines.push('{');
        // bound state setter
        lines.push(`    var setState = (...args) => this.setState(...args);`);
        // default destructure
        lines.push(`    var { props, state, context, constructor: klass } = this;`);
        // additional variable destructures
        getDestructures({ props, state, context, this: siht, klass }, lines.sub(1));
        // handle function body
        getBody(body, { loop }, lines.sub(1));
        // closing function brace
        lines.push('}');

        return lines;
    }

    parse.sfc = (node, lines = strings()) =>
    {
        var name = attrName(node, DEFAULT_COMPONENT_NAME);
        var siht = attrValue(node, 'this', '');
        var props = attrValue(node, 'props', '');
        var highOrder = attrValue(node, 'highOrder');
        var loop = attrValue(node, 'loop');
        var contexts = attrValue(node, 'contexts');
        var refs = attrValue(node, 'refs');
        var body = bodyContent(node);
        // render function signature
        lines.push(`function ${name}(props)`);
        // opening function brace
        lines.push('{');

        lines.subFn(1, lines =>
        {
            // add var destructuring
            getDestructures({ props, this: siht }, lines);
            // allow use of context hooks
            getUseContexts(contexts, lines);
            // allow generation of element refs
            getUseRefs(refs, lines);
            // allow for useState hooks
            getUseStates(node, lines);
            // handle function body
            getBody(body, { loop }, lines);
        });
        // closing function brace
        lines.push('}');
        // add HOC(s) if given
        getHighOrder(name, highOrder, lines);

        return lines;
    }

    var file = parse(getRootNode(content), JTM_TAGS);
    // all exports
    file.push(`export { ${exportArray.join(', ')} };`);
    // set the default export
    if (defaultExportName) file.push(`export default ${defaultExportName};`);

    return file.join();
}
