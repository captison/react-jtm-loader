## React JTM Loader Change Log

### In Development (TODOs)


### Releases

#### 0.3.2

- `bind` attribute added to `class` jtm tag for binding methods.
- `bind` attribute added to `render` jtm tag for binding (requires "class-properties" babel plugin).
- `contextType` attribute added to `class` for setting single context
- useContext hooks available for `sfc` tags
- removed documentation for `assign` tag (likely to be deprecated/removed in the future)


#### 0.3.1

- documentation updates


#### 0.3.0 - breaking changes

- `class` jtm tag added to support creation of react component class
    - `name` specifies the export name
    - `extends` specifies the class to extend
    - `initState` specifies the initial state for the component
    - `initProps` specifies the default props for the component
    - `highOrder` specifies functions used to be passed the component definition
    - `render` jtm tags in the body define class methods
- `context` jtm tag added to support creation of react context
    - `name` specifies the export name
    - `default` specifies the context default value
- `extender` jtm tag creates a function that adds functionality to prototypes
    - `name` specifies the export name
    - `prefix` specifies a prefix to use for `render` methods
    - `render` jtm tags in the body define extension methods
- added `highOrder` to `sfc` jtm tag
- useState hooks available for `sfc` tags
- useRef hooks available for `sfc` tags
- `render` jtm tag no longer prefixes generated function names with 'render'
- `render` jtm tag can no longer be used at root level
- `assemble` jtm tag removed
- fixed bug with `loop` (variable assignment)


#### 0.2.0 - breaking changes

- `assign` allows for variable assignment
- `extend()` has been removed
- `sfc` added for stateless functional components
- `assemble` adds support for default component/hof export
- high-order function export must now be explicitly specified


#### 0.1.0

- `extend()` is no longer needed as the generated template files can apply their own render methods


#### 0.0.3

- removed spread operator from `extend()` for more vanilla JS usage


#### 0.0.2

- fixed a compilation bug


#### 0.0.1

- first version of **React JTM Loader**!
